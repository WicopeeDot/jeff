module.exports = {
	interval: 20, //in seconds
	runAtStart: true, //run the task when bot is being started
	callback: async function() {
		if(!this.bot.client.user) return;
		let stat = this.bot.stageDev ? "IN A DEVELOPEMENT MODE" : this.bot.config.status[Math.floor(Math.random() * this.bot.config.status.length)]

		this.bot.client.user.setPresence({
			activity: {
				name: this.bot.config.prefix + 'help | ' + stat
			},
			status: 'online'
		})
	}
}
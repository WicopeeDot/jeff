module.exports = {
	aliases: ['ss'],
	desc: 'Screenshots the website.',
	callback: async function(msg,args,line) {
		let url = args[0]
		if(!url) return msg.channel.send(":interrobang: Invalid URL!");
		
		if(!/^https?:\/\//.test(args[0])) url = 'http://' + url;
		let parsedURL = this.url.parse(url)
		if(!parsedURL) return msg.channel.send(":interrobang: Invalid URL!");
		
		let resolved;
		try {
			resolved = await this.util.promisify(this.dns.lookup)(parsedURL.host);
		} catch (err) {
			let em = this.utils.buildErrEmbed("⁉ Hostname does not resolve.")
			return msg.channel.send({embed: em})
		}

		resolved = resolved.address

		if(resolved == 'localhost' || resolved == '127.0.0.1' || resolved == '::1') {
			let em = this.utils.buildErrEmbed("⁉ be sure to hit that like button for more epic content :DDDD.", 'pls dont screenshot my server')
			return msg.channel.send({embed: em})
		}
		
		let startedAt = Date.now();
		
		const pup = await this.pup.launch({
			args: ['--no-sandbox'],
			ignoreHTTPSErrors: true
		});
		
		const page = await pup.newPage();
		await page.setJavaScriptEnabled(true);
		await page.setViewport({
			width: 1920,
			height: 1080
		});
		
		await page.addStyleTag({
			path: "files/pup_cstyle.css"
		})
		
		try {
			await page.goto(url, {waitUntil: 'networkidle2'});
			const scr = await page.screenshot();
			pup.close();
			
			let embed = new this.lib.MessageEmbed();
			embed.attachFiles([{
				attachment: scr,
				name: 'ss.png',
			}])
			embed.setAuthor(url,undefined,url);
			embed.setImage('attachment://ss.png')
			embed.setFooter(`⏰ Took: ${this.prettyMs(Date.now() - startedAt)}`)
			
			msg.channel.send({
				embed: embed
			})
		} catch (err) {
			let embed = new this.lib.MessageEmbed();
			embed.setAuthor(url,undefined,url)
			embed.setFooter(`⏰ Took: ${this.prettyMs(Date.now() - startedAt)}`)
			
			let errObj = err.toString().split(':')
			errObj.shift()
			errObj = errObj.join(':').trim()
			msg.channel.send(":warning: Failed to screenshot the website: `" + errObj + "`",{
				embed: embed
			})
		}
	}
}
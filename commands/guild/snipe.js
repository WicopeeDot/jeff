module.exports = {
	desc: 'Shows deleted messages in the channel, where the command were ran in.',
	callback: async function (msg, line, args) {
		let txt = ""

		if(!this.sniped[msg.channel.id])
			txt = "No messages to snipe.";
		else {
			let c = 1;

			for(let message of this.sniped[msg.channel.id]) {
				txt = txt + c.toString() + ". **" + message.author.tag + "**: " + message.content + "\n"
				c = c + 1
			}
		}

		let embed = new this.lib.MessageEmbed()
		embed.setTitle('Deleted messages in #' + msg.channel.name)
		embed.setDescription(txt)
		embed.setColor(0x09a576)
		embed.setAuthor(msg.author.tag, msg.author.avatarURL({ format: 'png', size: 64 }))

		msg.reply({
			embed: embed
		})
	}
}
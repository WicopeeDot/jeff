const ServerConfigurer = require('../../const/ServerConfigurer.js')

module.exports = {
	desc: 'Sets prefix for the server/guild.',
	callback: async function(msg,args,line) {
		let serverConfig = new ServerConfigurer(this, msg.guild)
		await serverConfig.setupValues()

		if(!args[0]) {
			let prefix = await serverConfig.get('prefix') || this.config.prefix;

            msg.channel.send(`Current prefix for this server: \`${prefix}\`.`)
        } else {
			if(this.commandHandler.checkForServerAdmin(msg))
				return msg.channel.send("🔒 Regular members, who don't have permission `ADMINISTRATOR`, are not permitted to change prefix.")

			await serverConfig.set('prefix', args[0])
			msg.channel.send(":ok_hand:")
        }
	}
}
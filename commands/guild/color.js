async function cleanuproles(member) {
	if (member.guild.me.hasPermission("MANAGE_ROLES")) {
		await member.roles.filter(role => role.name.match("^#")).every(role => member.roles.remove(role))
	}
}

module.exports = {
	perms: ["MANAGE_ROLES"],
	aliases: ['c'],
	desc: 'Gives your nick colors. Usage: ;color (HEX [#FFFFFF], RGB [255,255,255] OR HSL [360,100,100])',
	callback: async function(msg,args,line) {
		line = line.trim()
		
		if (line == "") {
			await cleanuproles(msg.member);
			msg.reply(':ok_hand: Cleared colors.');
			return;
		}

		if (line == "clear") {
			if(this.commandHandler.checkForServerAdmin(msg))
                                return msg.channel.send("?? Regular members, who don't have permission `ADMINISTRATOR`, are not permitted to delete all the color roles.")
			
			await msg.guild.roles.filter(role => role.name.match("^#")).every(role => role.delete('request'))
			return msg.reply(':ok_hand:')
		}
		
		let color;

		if (line == "avatar") {
			let palette = await this.vibrant.from(msg.author.avatarURL).getPalette()
			let dominant =  palette.Vibrant ||
				palette.LightVibrant ||
				palette.LightMuted ||
				palette.DarkVibrant ||
				palette.DarkMuted ||
				palette.Muted
			color = this.color(dominant.getHex())
		} else {
			try { color = this.color(args[0]) } catch(err) {}

			if(!color) {
				if(!args[1] || !args[2]) return msg.reply('Are you trying to give me HEX? (`#FFFFFF`, not `FFFFFF`)');

				let x = parseInt(args[0]), 
					y = parseInt(args[1]), 
					z = parseInt(args[2])

				if(x <= 255 && y <= 255 && z <= 255)
					color = this.color.rgb(x,y,z);
				else if(x <= 360 && y <= 100 && z <= 100)
					color = this.color.hsl(x,y,z)
				else return msg.reply('Not accepted, give me HEX, RGB or HSL.')
			}
		}

		color = color.hex()

		await cleanuproles(msg.member)

		let role = msg.guild.roles.find(val => val.name.match('^' + color))

		if(!role) {
			role = await msg.guild.roles.create({data: {
				name: color,
				color: color,
				permissions: [],
				hoist: false
			}})

			let lowest = msg.guild.me.roles.filter(role => role.name !== "@everyone").sort((a,b) => b.position < a.position).array()[0].position;
			role.setPosition(lowest - 1);
			
		}

		try {
			await msg.member.roles.add(role)
			msg.reply(':ok_hand:')
		} catch(err) {
			let errObj = err.toString().split(':')
			errObj.shift()
			errObj = errObj.join(':').trim()

			msg.reply('error occured: `' + errObj + '`'); 
		}
	}
}

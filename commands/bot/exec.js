module.exports = {
	desc: 'Executes a command in bash environment.',
	ownerOnly: true,
	callback: async function(msg,args,line) {
		let startedAt = Date.now();

		let result = await this.util.promisify(this.childProcess.exec)(line);
		let took = `:stopwatch: Took: ${this.prettyMs(Date.now() - startedAt)}`;
		let res = '';
		
		if (result.stdout) res = res + `STDOUT:\`\`\`\n${result.stdout}\n\`\`\`\n`;
		if (result.stderr) res = res + `STDERR:\`\`\`\n${result.stderr}\n\`\`\`\n`;
		if (result.err) return msg.channel.send(`:interrobang: An error has occured!\`\`\`${result.err}\`\`\`\n${took}`);
		res = res + `${took}`;

		msg.channel.send(res.substring(0, 1970));
	}
}

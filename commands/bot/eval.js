module.exports = {
	desc: 'Evaluates JS code.',
	ownerOnly: true,
	callback: async function(msg,args,line) {
		let result = ''
		try {
			result = this.util.inspect(eval(line), {
				breakLength: 15
			});
		} catch (err) {
			result = err
		}

		msg.channel.send(result.toString().substring(0, 1970), {
			code: 'js'
		})
	}
}
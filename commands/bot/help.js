module.exports = {
	desc: 'Displays commands. You\'re using it right now.',
	callback: async function(msg,args,line) {
		let embed = new this.lib.MessageEmbed();
		embed.setColor(0x05c478);

		if(!args[0]) {
			embed.setTitle('[help cmd]')

			let CFL = this.utils.capitalizeFirstLetter;

			this.commands.forEach(function(val,key,map) {
				let ok = [];

				val.forEach(function(v,k,m) {
					ok.push(`\`${k}\``)
				});

				embed.addField(CFL(key), ok.join(', '))
			});
		} else {
			let command = await this.commandHandler.findCommand(args[0]);
			let txt = command == undefined ? `There is no such command \`${args[0]}\`.` : `\`${args[0]}\` - \`${command.desc}\``
			txt += command.nsfw ? "\nThis command is NSFW." : ""
			txt += command.ownerOnly ? "\nThis command is for bot controllers only." : ""
			txt += command.serverProtected ? "\nRegular members, who don't have permission `ADMINISTRATOR`, are not permitted to run this command." : ""
			embed.setDescription(txt)
		}

		msg.channel.send({embed: embed})
	}
}
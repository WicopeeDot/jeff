module.exports = {
	desc: 'Evaluates JS code in asynchronous function context.',
	ownerOnly: true,
	callback: async function(msg,args,line) {
		let AsyncFunction = Object.getPrototypeOf(async function() { }).constructor;
		try {
			let result = await new AsyncFunction(line).call(Object.assign(this, {msg: msg}))
			
			msg.channel.send(this.util.inspect(result, {
				breakLength: 15
			}).substring(0, 1970), {code: 'js'});
		} catch(err) {
			msg.channel.send(err.toString().substring(0, 1970), {code: 'js'});
		}
	}
}
module.exports = {
	desc: 'Ping-pong.',
	callback: async function(msg,args,line) {
		const start = Date.now();
		const message = await msg.channel.send(":rocket: Uh hold on a moment Kleiner...")
		
		const time = Math.round((Date.now() - start) / 2);
		message.edit(`:rocket: Pong! ${time}ms`);
	}
}
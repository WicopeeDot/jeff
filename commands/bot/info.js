module.exports = {
	aliases: ['about'], 
	desc: 'Displays info about the bot.',
	callback: async function(msg,args,line) {
		let embed = new this.lib.MessageEmbed();

		let time = Math.round((Date.now() - this.startedAt) / 2);
		let x = this.prettyMs(time);
		let owners = this.config.owners.map(x => `<@!${x}>`).join(', ')
		let versions = `Node.JS - ${process.version}\nDiscord.JS - v${this.lib.version}`

		embed.setTitle('ℹ Info')
		embed.setDescription('About the bot Rocket.')
		embed.addField('🖥 Controller(s)', owners)
		embed.addField('⏲ Uptime', x)
		embed.addField('📚 Using...', versions)
		embed.addField('🛰 Server status', 'http://status.wicopee.xyz')
		embed.addField('🎲 Half-Life 3 installed', 'false')
		embed.setFooter('HEAD ' + this.fs.readFileSync('.git/refs/heads/master'))
		embed.setColor(0x09a576)

		return msg.reply({ embed: embed })
	}
}
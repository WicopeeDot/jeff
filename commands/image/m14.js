module.exports = {
	aliases: ['mfourteen', 'm14gang', 'm14g'],
	desc: 'Adds watermark "this command was made by m14 gang".',
	callback: async function(msg,args,line) {
		let link = await this.utils.fetchImageFromMessage(msg,line);
		if(!link || link.length == 0) {
			let em = this.utils.buildErrEmbed("⁉ No link found")
			return msg.channel.send({ embed: em })
		}
		link = link[0]

		let Image = await this.canvas.loadImage(link)
		let Image2 = await this.canvas.loadImage("files/m14.jpg")
		if(Image.width < 128 || Image.height < 128) {
			let em = this.utils.buildErrEmbed("⁉ Too small image, enlarge before trying again.", "Minimum: 128px.")
			return msg.channel.send({ embed: em })
		}
		
		let a = 720;

		let w = Image.height / a * Image2.width
		let h = Image.height / a * Image2.height

		let canvas = this.canvas.createCanvas(Image.width, Image.height)
		let ctx = canvas.getContext('2d')
		ctx.drawImage(Image, 0, 0)
		ctx.drawImage(Image2, Image.width - w, Image.height - h, w, h)

		msg.channel.send({
			files: [{
				attachment: canvas.createPNGStream(),
				name: 'm14.png'
			}]
		})
	}
}
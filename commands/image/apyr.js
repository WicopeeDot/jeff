function promised(gmObj) {
	return new Promise((res,rej) => {
		gmObj.toBuffer('JPG', function(err,buff) {
			if(err) rej(err)

			res(buff)
		})
	})
}

module.exports = {
	desc: 'Adds "друг" text on the image.',
	callback: async function(msg,args,line) {
		let link = await this.utils.fetchImageFromMessage(msg,line);
		console.log(link)
		if(!link || link.length == 0) {
			let em = this.utils.buildErrEmbed("⁉ No link found")
			return msg.channel.send({ embed: em })
		}
		link = link[0]

		let Image = await this.canvas.loadImage(link)
		if(Image.width < 128 || Image.height < 128) {
			let em = this.utils.buildErrEmbed("⁉ Too small image, enlarge before trying again.", "Minimum: 128px.")
			return msg.channel.send({ embed: em })
		}
		
		let canvas = this.canvas.createCanvas(Image.width, Image.height)
		let ctx = canvas.getContext('2d')
		ctx.drawImage(Image, 0, 0)

		let txt = 'друг'
		let size = Image.height * 0.2

		ctx.font = size + 'px Arial';
		let mes = ctx.measureText(txt)
		let x = Image.width / 2 - mes.width / 2, 
			y = Image.height - size - size * 0.1;
		
		ctx.strokeStyle = 'black';
		ctx.fillStyle = 'white';
		ctx.lineWidth = size * 0.1;
		ctx.strokeText(txt, x, y)
		ctx.fillText(txt, x, y)

		let buffer = this.gm(canvas.createPNGStream()).contrast(1).convolve([[-1, -2, -1],0, 0.7, 0, [1, 2, 1]])
			.noise("Impulse").modulate(75,50).quality(5);
		buffer = await promised(buffer)

		msg.channel.send({
			files: [{
				attachment: buffer,
				name: 'Apyr.jpg'
			}]
		})
	}
}
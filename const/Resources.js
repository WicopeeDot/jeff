let dns = require('dns');
let fs = require('fs');
let pup = require('puppeteer');
let url = require('url');
let util = require('util');
let childProcess = require('child_process');
let path = require('path');
let prettyMs = require('pretty-ms')
let moment = require('moment')
let Color = require('color')
let Vibrant = require("node-vibrant")
let sentry = require('@sentry/node')
let logger = require('logger').createLogger();
let canvas = require('canvas')
let gm = require('gm')

const commandsDir = path.join(__dirname, '../commands/');
const tasksDir = path.join(__dirname, '../tasks/');

class Resources {
	constructor(bot) {
		this.bot = bot;
		
		this.bot.dns = dns;
		this.bot.fs = fs;
		this.bot.pup = pup;
		this.bot.url = url;
		this.bot.util = util;
		this.bot.childProcess = childProcess;
		this.bot.prettyMs = prettyMs;
		this.bot.moment = moment;
		this.bot.color = Color;
		this.bot.vibrant = Vibrant;
		this.bot.sentry = sentry
		this.bot.sentry.init({ dsn: this.bot.config.credentials.dsn })
		this.bot.logger = logger
		this.bot.canvas = canvas;
		this.bot.gm = gm;

		this.bot.canvas.registerFont('files/arial.ttf', { family: "Arial" })
		this.bot.canvas.registerFont('files/HelveticaNeueCyr-Light.otf', { family: "HelveticaNeue" })

		if(this.bot.config.stageDev) {
			this.bot.logger.setLevel('debug')
		}
		
		this.bot.ResourceLoader = this;
	}
	
	loadCommands() {
		console.log("Scanning for categories > commands...")
		this.bot.commands = new this.bot.lib.Collection();
		let comms = this.bot.commands;

		let x = fs.readdirSync(commandsDir).filter(x => fs.lstatSync(commandsDir + x).isDirectory())
		
		for(let dir of x) {
			try {
				comms.set(dir, new this.bot.lib.Collection());
				let cat = comms.get(dir)

				for(let file of fs.readdirSync(commandsDir + dir)) {
					if(!file.endsWith('.js')) continue;
					file = file.split('.')[0] || file;

					this.bot.logger.info('including ' + dir + ' > ' + file)

					let command = require(`../commands/${dir}/${file}`);
					
					if(!cat) {
						throw new Error('no category, excuse me what')
					}

					cat.set(file, command);

					if(command.aliases) {
						for(const alias of command.aliases) {
							cat.set(alias,Object.assign({
								alias: true,
								aliasFor: file
							}, command))
						}
					}
				}
			} catch (err) {
				console.error(err, 'Failed to load category ' + dir)
			}
		}
	}
	
	loadTasks() {
		console.log("Loading tasks...")
		this.bot.tasks = new this.bot.lib.Collection();
		let tasks = this.bot.tasks;
		
		for (let file of fs.readdirSync(tasksDir)) {
			if(!file.endsWith('.js')) continue;
			file = file.split('.')[0] || file;
			
			try {
				let task = require('../tasks/' + file)
				task.interval = (task.interval || 5) * 1000; //cpnvert this shit to milliseconds
				tasks.set(file,task);
				let call = () => {
					tasks.get(file).callback.call(this).catch((err) => {
						this.bot.utils.consoleError(err, 'Failed to run task ' + file)
					});
				}
				if (task.runAtStart) call();
				
				setInterval(call, task.interval)
			} catch (err) {
				console.error(err, 'Failed to load task ' + file)
			}
		}
	}
}

module.exports = Resources;

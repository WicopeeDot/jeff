const EmoteParser = require('./EmoteParser.js')
const {URL} = require('url');
const urlRegex = require('url-regex');
const normalizeUrl = require('normalize-url');
const isImageUrl = require('is-image-url');

function getUrlsFromQueryParams(url) {
	const ret = new Set();
	const {searchParams} = (new URL(url));

	for (const [, value] of searchParams) {
		if (urlRegex({exact: true}).test(value)) {
			ret.add(value);
		}
	}

	return ret;
}

function isWhitespace(c) {
	return /\s|,/.test(c);
};

function special(c) {
	return /\s|"|'|,/.test(c);
};

function isValid(index, arr) {
	var counter = 0;

	while (true) {
		if (index - 1 - counter < 0) {
			break;
		}

		if (arr[index - 1 - counter] === '\\') {
			counter++;
			continue;
		}

		break;
	}

	return counter % 2 === 0;
}

class Utils {
	constructor(bot) {
		this.emoteParser = new EmoteParser();
		this.bot = bot;

		this.bot.sniped = {};
	}
	
	isOwner(id) {
		return this.bot.config.owners.includes(id);
	}

	capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
	
	registerHandlers() {
		this.bot.client.on('error', (err) => {
			this.bot.logger.error('Catched error for Discord client: ' + err.toString())
		})
		
		this.bot.client.on('warn', (warn) => {
			this.bot.logger.warn(`Warning: ${warn}`)
		})
		
		this.bot.client.on('rateLimit', (obj) => {
			this.bot.logger.warn(`Rate-Limited: ${obj}`)
		})
		
		this.bot.client.on('ready', () => {
			this.bot.logger.info("Bot is online! Censoring credentials...")
			this.bot.config.credentials = {};
		})

		let copy = this;

		this.bot.client.on('messageDelete', msg => {
			if(!copy.bot.sniped[msg.channel.id])
				copy.bot.sniped[msg.channel.id] = [];
			else if(copy.bot.sniped[msg.channel.id].length == 5)
				copy.bot.sniped[msg.channel.id][5] = undefined;

			copy.bot.sniped[msg.channel.id].unshift(msg);
		})
	}

	hasEmotes(txt) {
		console.log(txt, txt.match(this.emoteParser.customEmoteRegex) != null) 
		return this.emoteParser.emoteRegex.test(txt) || txt.match(this.emoteParser.customEmoteRegex) != null
	}

	async fetchImageFromMessage(msg,line,israrepepe = false) {
		let imageURLs = this.getUrls(line);

		for (const embed of msg.embeds) {
			if (embed.type == 'image') {
				imageURLs.push(embed.url);
			}

			if (embed.type == 'rich' && embed.image) {
				imageURLs.push(embed.image.url);
			}
		}

		for (const attachment of msg.attachments.values()) {
			if (attachment.size === 0 || !attachment.width || !attachment.height) continue;

			imageURLs.push(attachment.url);
		}

		if(!israrepepe) {
			let matchedUser = this.getMemberFromMessage(msg);
			if (matchedUser) {
				imageURLs.push(matchedUser.user.avatarURL({ format: 'png', size: 2048 }));
			}

			let emote = this.emoteParser.getEmoteURL(msg.content);
			if (emote) {
				imageURLs.push(emote);
			}

			let fetched = Array.from(msg.channel.messages.values()).sort((x,y) => y.createdTimestamp - x.createdTimestamp)
			for(let a of fetched) {
				if(a.id != msg.id) {
					let arr = await this.fetchImageFromMessage(a,a.content,true)
					imageURLs = imageURLs.concat(arr);
				}
			}

			console.log('epic rare pepe found!')
			imageURLs.push(msg.author.avatarURL({ format: 'png', size: 2048 }))
		}

		return imageURLs
	}

	getMemberFromMessage(msg) {
		if (!msg) return;

		let recentTimestamp = 0;
		let match;

		for (const member of msg.guild.members.array()) {
			if (!(member.user.tag.toLowerCase().includes(msg.content.toLowerCase())) && !(member.nickname && member.nickname.toLowerCase().includes(msg.content.toLowerCase())) && !(member.user.id === msg.content.replace(/[^\d]/g, '')) || ((member.lastMessage ? member.lastMessage.createdTimestamp : 0) < recentTimestamp)) continue;

			recentTimestamp = member.lastMessage ? member.lastMessage.createdTimestamp : 0;
			match = member;
		}

		return match;
	}

	buildErrEmbed(errStr, footer) {
		let embed = new this.bot.lib.MessageEmbed();

		embed.setDescription(errStr);
		if(footer) embed.setFooter(footer)
		embed.setColor(0xbf0909)

		return embed
	}

	consoleError(err, msg) {
		let event = this.bot.sentry.captureException(err)
		this.bot.logger.error(msg + " Event " + event)
	}

	getUrls(text, options = {}) {
		if (typeof options.exclude !== 'undefined' && !Array.isArray(options.exclude)) {
			throw new TypeError('The `exclude` option must be an array');
		}

		const ret = new Set();

		const add = url => {
			ret.add(normalizeUrl(url.trim().replace(/\.+$/, ''), options));
		};

		const urls = text.match(urlRegex()) || [];
		for (const url of urls) {
			if(isImageUrl(url)) {
				add(url);

				if (options.extractFromQueryString) {
					for (const qsUrl of getUrlsFromQueryParams(url)) {
						add(qsUrl);
					}
				}
			}
		}

		for (const excludedItem of options.exclude || []) {
			for (const item of ret) {
				const regex = new RegExp(excludedItem);
				if (regex.test(item)) {
					ret.delete(item);
					break;
				}
			}
		}

		return Array.from(ret);
	};
	
	parseArgs(input) {
		/*
			https://github.com/txgruppi/parseargs.js
			The MIT License (MIT)
			Copyright (c) 2015 Tarcísio Gruppi
			Permission is hereby granted, free of charge, to any person obtaining a copy
			of this software and associated documentation files (the "Software"), to deal
			in the Software without restriction, including without limitation the rights
			to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
			copies of the Software, and to permit persons to whom the Software is
			furnished to do so, subject to the following conditions:
			The above copyright notice and this permission notice shall be included in all
			copies or substantial portions of the Software.
			THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
			IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
			FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
			AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
			LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
			OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
			SOFTWARE.
		*/
		if (typeof input !== 'string') {
			throw new Error('Invalid input type');
		}

		var reading = false;
		var startChar = null;
		var startIndex = null;

		var read = function(s, e) {
			reading = false;
			startChar = null;
			startIndex = null;
			return input.substring(s, e);
		}

		var result = Array.prototype.reduce.call(
			input.trim(),
			function(result, value, index, arr) {
				if (reading && startChar === ' ' && special(value) && !isWhitespace(value)) {
					throw new Error("Invalid argument(s)");
				}

				if (! (reading || special(value))) {
					reading = true;
					startChar = ' ';
					startIndex = index;

					if (index === arr.length - 1 && startChar === ' ') {
						return result.concat([
						read(startIndex)
						]);
					}

					return result;
				}

				if (!reading && special(value) && !isWhitespace(value)) {
					reading = true;
					startChar = value;
					startIndex = index;
					return result;
				}

				if (!reading) {
					return result;
				}

				if (startChar === ' ' && isWhitespace(value)) {
					if (!isValid(index, arr)) {
						throw new Error("Invalid syntax");
					}

					return result.concat([read(startIndex, index)]);
				}

				if (startChar === value && special(startChar) && isValid(index, arr)) {
					return result.concat([
						read(startIndex + 1, index)
					]);
				}

				if (index === arr.length - 1 && startChar === ' ') {
					return result.concat([
						read(startIndex)
					]);
				}

				return result;
			},
			[]
		);

		if (startIndex || startChar) {
			throw new Error('Unexpected end of input');
		}

		return result.map(function(str) {
		return str.replace(/\\([\s"'\\])/g, '$1');
		});
	}
}

module.exports = Utils;

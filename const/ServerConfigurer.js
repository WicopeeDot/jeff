class ServerConfigurer {
    constructor(bot, guild) {
        this.guild = guild;
        this.bot = bot;
    }

    get(name) {
        return this.document[name] || false;
    }

    async set(name, value) {
        let opts =  { $set: {} }
        opts.$set[name] = value
        
        let res = await this.collection.updateOne(this.document, opts);
        if(!res.message.hashedName) throw new Error("wait what how did it refuse to update one");
        res = { _id: res.message.hashedName }

        this.document = res;
    }

    async setupValues() {
        const db = this.bot.database.db
		this.collection = db.collection('settings');
        
        let res = await this.collection.find({ _SVID: this.guild.id }).toArray()
        if(!res || !res[0]) {
            let id = await this.collection.insertOne({ _SVID: this.guild.id }).insertedId
            if(!id) throw new Error("Failed to create or find the document.")
            
            res = [ { _id: id, _SVID: this.guild.id } ];
        }
        res = res[0]

        this.document = res;
    }
}

module.exports = ServerConfigurer;
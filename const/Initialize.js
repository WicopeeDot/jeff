
const discord = require('discord.js');

const Commands = require('./Commands.js');
const Resources = require('./Resources.js');
const Database = require('./Database.js');
const Utils = require('./Utils.js');
const ReactionList = require('./ReactionList.js');

class Init {
	constructor(config) {
		if(!config) {
			console.log("Config file is missing! (config.json)")
			return process.exit(1);
		}
		
		this.bot = {};
		this.bot.stageDev = config.stageDev;
		this.bot.startedAt = Date.now();
		this.bot.client = new discord.Client();
		this.bot.lib = discord;
		this.bot.config = config;

		if(this.bot.stageDev) {
			console.log('====[RUNNING IN STAGEDEV MODE]===')
		}
		
		this.bot.commandHandler = new Commands(this.bot);
		this.bot.ReactionList = ReactionList;
		this.bot.commandHandler.registerHandlers();
		this.bot.ResourceLoader = new Resources(this.bot);
		this.bot.ResourceLoader.loadCommands();
		this.bot.ResourceLoader.loadTasks();
		this.bot.utils = new Utils(this.bot);
		this.bot.utils.registerHandlers(); //misc handlers
		this.bot.database = new Database(this.bot)
		
		this.bot.client.login(this.bot.config.credentials.discord);
	}
}

module.exports = Init;

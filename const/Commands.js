const ServerConfigurer = require('./ServerConfigurer.js')

class Commands {
	constructor(bot) {
		this.bot = bot;
	}
	
	async findCommand(name) {
		let obj;

		this.bot.commands.forEach(function(v,k) {
			v.forEach(function(val,key) {
				if(key == name) {
					obj = val;
				}
			})
		})

		return obj
	}

	async getPrefix(guild) {
		let svconfig = new ServerConfigurer(this.bot, guild);
		await svconfig.setupValues()
		let prefix = await svconfig.get('prefix');

		return prefix || this.bot.config.prefix
	}

	checkForServerAdmin(msg) {
		return !this.bot.utils.isOwner(msg.author.id) && !msg.member.hasPermission("ADMINISTRATOR")
	}

	async handleMessage(msg) {
		if (msg.author.bot) return;
		let args = msg.content.split(' ')
		
		let name;
		let prefix = await this.getPrefix(msg.guild)
		
		if(args[0].startsWith(prefix)) {
			if(!msg.guild)
				return msg.channel.send(":interrobang: Commands cannot be used in DMs, sorry about that.");

			if(this.bot.stageDev && !this.bot.utils.isOwner(msg.author.id))
				return msg.channel.send(":interrobang: IN A DEVELOPEMENT MODE, SORRY!");
			
			name = args[0].substring(prefix.length,args[0].length);
			if(!name) return;
			
			name = name.toLowerCase();
			args.shift();
			var line = args.join(' ')
			try{ args = this.bot.utils.parseArgs(line) } catch(err) { }
			
			let command = await this.findCommand(name);

			if(command) {
				if(command.ownerOnly && !this.bot.utils.isOwner(msg.author.id))
					return msg.channel.send("🔒 You are not permitted to run this command.");

				if(command.nsfw && !msg.channel.nsfw)
					return msg.channel.send("🔞 This command is not eligible to run in non-NSFW channels.")

				if(command.serverProtected && this.checkForServerAdmin(msg))
					return msg.channel.send("🔒 Regular members, who don't have permission `ADMINISTRATOR`, are not permitted to run this command.")

				if(command.perms) {
					let noperms = [];
					command.perms.forEach(e => {
						if(!msg.guild.me.hasPermission(e)) {
							noperms.push(`\`${e}\``)
						}
					});

					if(noperms.length != 0) {
						return msg.channel.send(`⁉ The bot does not have permissions ${noperms.join(',')}. Contact server administrator.`)
					}
				}
				
				this.bot.logger.info(`${msg.author.username}#${msg.author.discriminator} ran a command "${name}", with args: ${args.join(', ')}`)
				
				msg.channel.startTyping();
				command.callback.call(this.bot,msg,args,line).catch(async (err) => {
					let eventId = this.bot.sentry.captureException(err)
					let errObj = err.toString().split(':')
					errObj.shift()
					errObj = errObj.join(':').trim()
					let embed = await this.bot.utils.buildErrEmbed(`⁉ Internal error occured, try again later.\n\`${errObj}\``, `Event ${eventId}`)

					this.bot.logger.error('Catched error for command ' + name + ', event ' + eventId)

					msg.channel.send({ embed: embed })
				});
				msg.channel.stopTyping(true);
			}
			
		}
	}
	
	registerHandlers() {
		this.bot.client.on('message', (msg) => {
			this.handleMessage(msg);
		})
	}
}

module.exports = Commands;

const client = require('mongodb').MongoClient;

class Database {
	constructor(bot) {
		this.bot = bot;
		this.client = client;
		this.Initialize()
	}

	Initialize() {
		let cred = this.bot.config.credentials;
		let url = `mongodb://${cred.dbuser}:${cred.dbpass}@${cred.dbhost}:${cred.dbport}/?authMechanism=DEFAULT&authSource=rocketbot`;

		let dis = this;

		this.client.connect(url, function(err,db) {
			if(err)
				dis.bot.utils.consoleError(err, 'Failed to connect to the database (server down??)')

			dis.db = db.db('rocketbot');
			
		})
	}
}

module.exports = Database;
